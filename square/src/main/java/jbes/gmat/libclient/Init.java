package jbes.gmat.libclient;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.WatchEvent;
import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.json.Json;

import static java.nio.file.StandardWatchEventKinds.*;

public class Init {
	private final long id;
	private final String app;
	private final String port;
	private final String servicePort;
	private final String dockerInstance;
	
	public Init(Long id, String app, String port, String servicePort, String dockerInstance) {
		this.id = id;
		this.app = app;
		this.port = port;
		this.servicePort = servicePort;
		this.dockerInstance = dockerInstance;
	}

	private void watcherLogs(WatchService watcher) {
		for (;;) {
			WatchKey key;
			try {
				key = watcher.take();
			} catch (InterruptedException x) {
				return;
			}

			for (WatchEvent<?> event: key.pollEvents()) {
				WatchEvent.Kind<?> kind = event.kind();

				// This key is registered only
				// for ENTRY_CREATE events,
				// but an OVERFLOW event can
				// occur regardless if events
				// are lost or discarded.
				if (kind == OVERFLOW) {
					continue;
				}
				
				var uri = URI.create("logs/logs").getPath();
				//var file = new File(uri);
				//System.out.println(Arrays.toString(file.list()));
				try (var reader = Files.newBufferedReader(Paths.get(uri))) {
					while(true) {
						String s = reader.readLine();
						if (s != null) {
							String JSON = getJsonFormatted(s);
							sendHttpRequest(JSON);
						}
					}
				} catch (IOException e) {
					System.out.println("watcher : " + e.getClass() + ", cause : " + e.getMessage());
				}
			}

			// Reset the key -- this step is critical if you want to
			// receive further watch events.  If the key is no longer valid,
			// the directory is inaccessible so exit the loop.
			if (!key.reset()) break;
		}
	}

	private String getJsonFormatted(String message) {
		var timestamp = new Date().getTime();
		var jsonToBuild = Json.createObjectBuilder()
				 .add("id", id)
				 .add("app", app)
			     .add("port", port)
			     .add("servicePort", servicePort)
			     .add("dockerInstance", dockerInstance)
			     .add("message", message)
			     .add("timestamp", timestamp);
		return jsonToBuild.build().toString();
	}
	
	private WatchService initWatcher(String path) throws IOException {
		WatchService watcher = FileSystems.getDefault().newWatchService();
		Path dir = Paths.get(path);
		try {
			dir.register(watcher, ENTRY_MODIFY);
		} catch (IOException x) {
			System.err.println(x);
		}
		return watcher;
	}

	private static void sendHttpRequest(String Json) {
		try {	
			URL url = new URL("http://172.17.0.1:8080/logs/utils/insert");
			
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection) con;
			http.setRequestMethod("POST");
			http.setDoOutput(true);
			
			byte[] out = Json.getBytes(StandardCharsets.UTF_8);
			int length = out.length;
			
			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-type", "application/json; charset=UTF-8");
			http.connect();
			try (OutputStream os = http.getOutputStream()) {
				os.write(out);
			}
		} catch (IOException e) {
			System.out.println("request : " + e.getMessage());
		}
	}

	public static void main(String[] args) {
		var init = new Init(Long.valueOf(args[0]), args[1], args[2], args[3], args[4]);
		try {
			WatchService watcher = init.initWatcher("logs");
			init.watcherLogs(watcher);
		} catch(IOException e) {
			System.out.println("error while parsing logs file : " + e.getMessage());
		}
	}
}
