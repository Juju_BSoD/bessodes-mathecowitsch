package jbes.gmat.square;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonBuilderFactory;

/**
 * This class allows to manage the applications by creating a List of Application.
 * 
 * <ul>
 * <li>id is a parameter giving an information of the id of each instance of an application and incrementing at each one.</li>
 * <li>servicePort is the first service Port used by the server and incrementing at each new instance.</li>
 * <li>apps is the list of the different instances organized by applications.</li>
 * </ul>
 * 
 * @author G.Mathecowitsch J.Bessodes
 *
 */
@ApplicationScoped
public class ApplicationManager {
	/**
	 * id is a parameter giving an information of the id of each instance of an application and incrementing at each one.
	 */
	private long id = 1;
	
	/**
	 * servicePort is the first service Port used by the server and incrementing at each new instance.
	 */
	private long servicePort = 1024;
	
	/**
	 * apps is the list of the different instances organized by applications.
	 */
	private HashMap<String, List<Application>> apps;
	
	/**
	 *  <b>Constructor of ApplicationManager</b>
	 */
	public ApplicationManager() {
		apps = new HashMap<>();
	}
	
	/**
	 * This method add an application to the apps list if absent.
	 * 
	 * @param app
	 * 		app is the application to add to the list.
	 */
	public void add(Application app) {
		Objects.requireNonNull(app);
		apps.computeIfAbsent(app.getApplicationName(), (list) -> new ArrayList<>()).add(app);
	}
	
	/**
	 * This methode remove an app from the apps list.
	 * 
	 * @param app
	 * 		app is the application to remove from the list.
	 */
	public void remove(Application app) {
		Objects.requireNonNull(app);
		String key = app.getApplicationName();
		List<Application> list;
		if ((list = apps.get(key)) != null) {
			list.remove(app);
		}
	}
	
	/**
	 * This method is used to know the next free ID to use.
	 * 
	 * @return
	 * 		This method returns the next usable id.
	 */
	public long getNextId() {
		return id++; //return then increments
	}
	
	/**
	 * This method is used to know the next free service port to use.
	 * 
	 * @return
	 * 		This method returns the next usable service port.
	 */
	public long getNextServicePort() {
		return servicePort++; //return then increments
	}
	
	/**
	 * This method is used to know the instance name of an application.
	 * 
	 * @param appName
	 * 		appName is the name of the application on which you want informations.
	 * @return
	 * 		This method returns the name of the docker instance of the application.
	 */
	public String getDockerInstanceName(String appName) {
		List<Application> appList;
		if (null == (appList = apps.get(appName))) {
			return appName + "-1";
		}
		else {
			return appName + "-" + (appList.size() + 1);
		}
	}
	
	/**
	 * This method is used to find an application with an ID.
	 * 
	 * @param id
	 * 		id is the id of the application to find.
	 * @return
	 * 		This method returns the application corresponding to the id.
	 */
	public Application getAppById(long id) {
		for(List<Application> list: apps.values()) {
			for(Application app : list) {
				if(app.getId()==id) {
					return app;
				}
			}
		}
		throw new IllegalStateException("no such app");
	}

	/**
	 * This method allows to find an instance of an application if it exists.
	 * 
	 * @param instanceName
	 * 		instanceName is the name of the instance to find.
	 * @return
	 * 		this method returns the concerned application if found or an Optional.empty if not.
	 */
	public Optional<Application> getApp(String instanceName) {
		String appName = instanceName.split("-", 2)[0];
		if (apps.containsKey(appName)) {
			return apps.get(appName).stream().filter(e -> e.getDockerInstance().equals(instanceName)).findFirst();
		}
		else
			return Optional.empty();
	}
	
	/**
	 * This method is used to check the running apps and update the list.
	 */
	public void checkContainer() {
		try {
			var list = SquareProcess.getRunningAppNames();
			var newApps = new HashMap<String, List<Application>>();
			list.forEach(elem -> {
				getApp(elem).ifPresent(x -> newApps.computeIfAbsent(x.getApplicationName(), (value) -> new ArrayList<>()).add(x));
			});
			apps = newApps;
		} catch (IOException e) {
			System.out.println("problem while updating apps");
		}
	}
	
	/**
	 * This method is used to create a JSON Object of the list of running applications.
	 * 
	 * @return
	 * 		This method returns a JSON of the list of running applications.
	 */
	public JsonArray getList() {
		JsonBuilderFactory factory = Json.createBuilderFactory(null);
		var array = factory.createArrayBuilder();
		apps.forEach((key, list) -> {
			list.forEach(app -> {
				array.add(app.toJSON(true));
			});
		});
		return array.build();
	}
}
