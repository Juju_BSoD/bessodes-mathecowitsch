package jbes.gmat.square;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonBuilderFactory;

/**
 * A class allowing to create a DataBase used to manage logs.
 * <ul>
 * <li>connectionDB is the object supporting SQL statements specific to our created Database.</li>
 * </ul>
 * @author J.Bessodes G.Mathecowitsch 
 */
public class DataBaseManager {
	/**
	 * connectionDB is the object supporting SQL statements specific to our created Database.
	 */
	private final Connection connectionDB;

	/**
	 * <b>Constructor of DataBaseManager</b>
	 * 
	 * @param connectionDB
	 * 		connectionDB is the object Connection of this DataBase.
	 */
	private DataBaseManager(Connection connectionDB) {
		this.connectionDB = connectionDB;
	}
	
	/**
	 * This method allows to create a new Database in the file logs.db using SQL Connection and the SQLite-JDBC driver. 
	 * 
	 * @return
	 * 		This method create a new DataBaseManager using the constructor or this class. If there is an error catched it returns null.
	 */
	public static DataBaseManager create() {
		try {
			Class.forName("org.sqlite.JDBC");
			Connection connection = DriverManager.getConnection("jdbc:sqlite:" + System.getProperty("user.dir") + "/../../logs/logs.db");
			return new DataBaseManager(connection);
		} catch (SQLException e) {
			System.err.println("unable to create a connection to the Data Base");
		} catch (ClassNotFoundException e) {
			System.err.println("unable to find jdbc-sqlite driver");
		}
		return null;
	}
	
	/**
	 * This method allows to flush the current Database.
	 */
	public void flush() {
		try {
			var statement = connectionDB.createStatement();
			statement.executeUpdate("DELETE from logs WHERE 1");
		}
		catch(SQLException e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * This method allows to insert a new logs with differents application informations.
	 * 
	 * @param id
	 * 		id is the id of the application concerned by this log.
	 * @param app
	 * 		app is the name of the application concerned by this log.
	 * @param port
	 * 		port is the port of the application concerned by this log.
	 * @param servicePort
	 * 		servicePort is the service port of the application concerned by this log.
	 * @param dockerInstance
	 * 		dockerInstance is the name of the instance of the application concerned by this log.
	 * @param message
	 * 		massage is the content and the information about the application concerned by this log.
	 * @param timestamp
	 * 		timestamp is the time of the information concerned by this log.
	 */
	public void insert(long id, String app, String port, String servicePort, String dockerInstance, String message, long timestamp) {
		try {
			var statement = connectionDB.createStatement();
			statement.executeUpdate(String.format("insert into logs values(%d, '%s', '%s', '%s', '%s', '%s', %d)", id, app+":"+port, port, servicePort, dockerInstance, message, timestamp));
		}
		catch(SQLException e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * This method allows to filter the logs of the Database to find a specific one.
	 * 
	 * @param time
	 * 		time you want to use in your filter.
	 * @param filter
	 * 		the filter you want to apply on your database
	 * @return
	 */
	public JsonArray getLogsFiltered(Integer time, String filter) {
		ResultSet rs = null;
		JsonBuilderFactory factory = Json.createBuilderFactory(null);
		var array = factory.createArrayBuilder();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		try {
			var statement = connectionDB.createStatement();
			long now = new Date().getTime();
			if (filter == null) rs = statement.executeQuery(String.format("select * from logs where logs.timestamp > %d ORDER BY logs.timestamp", now - (time * 60000)));
			else rs = statement.executeQuery(String.format("select * from logs where logs.timestamp > %d AND (id == '%s' OR appName == '%s' OR dockerInstanceName == '%s') ORDER BY logs.timestamp", now - (time * 60000), filter, filter, filter));
			while(rs.next()) {
				var jsonToBuild = Json.createObjectBuilder()
					     .add("id", rs.getInt("id"))
					     .add("app", rs.getString("appName"))
					     .add("port", rs.getString("port"))
					     .add("service-port", rs.getString("servicePort"))
					     .add("docker-instance", rs.getString("dockerInstanceName"))
					     .add("message", rs.getString("message"))
					     .add("timestamp", format.format(new Date(rs.getLong("timestamp"))))
					.build();
				array.add(jsonToBuild);
			}
		} catch (SQLException e) {
			System.out.println("error getLogsFiltered : " + e.getMessage() + ", state : " + e.getSQLState());
		}
		return array.build();
	}
}