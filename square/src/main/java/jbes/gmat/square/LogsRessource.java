package jbes.gmat.square;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;

/**
 * A class allowing to execute the various commands related to the functions of the server.
 * 
 * @author J.Bessodes G.Mathecowitsch 
 */
@Path("/logs")
public class LogsRessource {
	
	DataBaseManager db = DataBaseManager.create();
	
	@GET
	@Path("/{time}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
    public Response log(@PathParam("time") Integer time) {
		return this.log(time, null);
    }
	
	@GET
	@Path("/{time}/{filter}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
    public Response log(@PathParam("time") Integer time, @PathParam("filter") String filter) {
		var results = db.getLogsFiltered(time, filter);		//filter might be null
        return Response.ok(results).build();
    }
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/utils/insert")
	public Response insert(JsonObject js) {
		Objects.requireNonNull(js);
		try {
			db.insert(Long.valueOf(js.get("id").toString()),
					js.get("app").toString().replace("\"", ""),
					js.get("port").toString().replace("\"", ""),
					js.get("servicePort").toString().replace("\"", ""),
					js.get("dockerInstance").toString().replace("\"", ""),
					js.get("message").toString().replace("\"", ""),
					Long.valueOf(js.get("timestamp").toString())
			);
			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("erreur : " + e.getMessage() + ", nature : " + e.getClass());
		}
		return Response.serverError().build();
	}
	
	@GET
	@Path("/utils/flush")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response flush() {
		db.flush();
		return Response.ok().build();
	}
	
	@GET
	@Path("/utils/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response insertDataSet() {
		long now = new Date().getTime();
		db.insert(1L, "demo", "8082", "15028", "demo-1", 	"message test - demo-1", 		now);
		db.insert(2L, "demo", "8082", "15029", "demo-2", 	"message test - demo-2", 		now - 60000);
		db.insert(3L, "pingpong", "8083", "15030", "pingpong-1", 	"message test - pingpong-1", 	now - 6000000);
		return Response.ok().build();
	}
}
