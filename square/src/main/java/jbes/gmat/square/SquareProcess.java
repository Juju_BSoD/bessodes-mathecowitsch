package jbes.gmat.square;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * A class allowing to execute the various commands related to the functions of the server.
 * @author G.Mathecowitsch J.Bessodes
 */

public class SquareProcess {
	
	/**
	 * This method allows you to create a new file named dockerfile within the "apps" directory.
	 * @return this method return true if the creation of the new file "Dockerfile" worked. Otherwise it returns false.
	 */
	private static boolean createFile() {
		try {
			File file = new File("../../apps/Dockerfile");
			if(!file.createNewFile()) 
				return false;
		} catch(IOException e) {
			System.out.println("Exception occurred when creating file");
		}
		return true;
	}

	/**
	 * This method allows you to write the different fields needed in the dockerfile to build an image docker.
	 * 
	 * @param app
	 * 		app is the application you want to create a dockerfile for.
	 */
	private static void writeFile(Application app) {
		try (PrintWriter printWriter = new PrintWriter("../../apps/Dockerfile")) {
			printWriter.println("FROM openjdk:11");
			printWriter.println("WORKDIR /");
			printWriter.printf("COPY apps/%s.jar %s.jar\n", app.getApplicationName(), app.getApplicationName());
			printWriter.printf("COPY lib-client/lib-client.jar lib-client.jar\n");
			printWriter.printf("COPY docker-images/script.sh script.sh\n");
			printWriter.printf( "RUN [\"chmod\",\"+x\",\"lib-client.jar\"]\n" +                            // Give permission to execute
		             			"RUN [\"chmod\",\"+x\",\"%s.jar\"]\n" +
		             			"RUN [\"chmod\",\"+x\",\"script.sh\"]\n", app.getApplicationName());
			printWriter.printf("EXPOSE %s\n", app.getPort());
			printWriter.printf("CMD bash script.sh %s %s %s %s %s\n", app.getId(), app.getApplicationName(), app.getPort(), app.getServicePort(), app.getDockerInstance());
			printWriter.close();
		} catch (FileNotFoundException e) {
			System.out.println("Exception occurred : file not found when writing Dockerfile");
		}
	}

	/**
	 * This method allows you to create an application's dockerfile by creating a file and writing it.
	 * 
	 * @param app
	 * 		app is the application you want to create a dockerfile for.
	 */

	private static void createDockerfile(Application app) {
		if(createFile() == false) {
			File f= new File("../../apps/Dockerfile");
			f.delete();
			createDockerfile(app);
		} else {
			writeFile(app);
		}
		return;
	}

	/**
	 * This method allows to build an application in image docker in order to be able to launch it in docker containers.
	 * 
	 * @param name
	 * 		name is the name of the application you want to build into a docker image.
	 */
	private static void buildWithDocker(String name) {
		List<String> command= Arrays.asList("docker", "build", "-f", "../../apps/Dockerfile", "-t", name, "../../");
		//System.out.println("docker build -f ../apps/Dockerfile -t "+ name+" .");
		ProcessBuilder pb = new ProcessBuilder(command);
		try {
			Process process = pb.start();
			process.waitFor();
		} catch (IOException | InterruptedException e) {
			System.out.println("Exception occurred when building docker");
		}
	}
	
	/**
	 * This method allows to launch an application in operation in a container with an existing image docker.
	 * 
	 * @param instanceName
	 * 		instanceName is the name of the specific instance of a dockerizable application you want to run.
	 * @param port
	 * 		port is the specific port you want to use to run your application.
	 * @param servicePort
	 * 		servicePort is the specific service Port you want to use to run your application.
	 * @param name
	 *  	name is the name of the application you want to run.
	 */
	public static void run(String instanceName, String port, String servicePort, String name) {
		StringBuilder sp= new StringBuilder();
		sp.append(servicePort).append(":").append(port); 
		List<String> command= Arrays.asList("docker", "run", "-d", "-it", "--rm", "--name", instanceName, "-p", sp.toString(), name);
		ProcessBuilder pb = new ProcessBuilder(command);
		try {
			Process process = pb.start();
			process.waitFor();
		} catch (IOException | InterruptedException e) {
			System.out.println("Exception occurred when running a docker container");
		}
	}

	/**
	 * This method allows to deploy an application by creating the specific Dockerfile, building it into a docker image and running it.
	 * 
	 * @param app
	 * 		app is the specific application you want to deploy. 
	 */
	public static void deploy(Application app) {
		createDockerfile(app);
		buildWithDocker(app.getApplicationName());
		run(app.getDockerInstance(), String.valueOf(app.getPort()), String.valueOf(app.getServicePort()), app.getApplicationName());
		return;
	}
	
	/**
	 * This method allows to list any application currently running on docker.
	 * 
	 * @return
	 * 		This method return an ArrayList with the name of every application currently running.
	 * @throws IOException
	 */
	public static List<String> getRunningAppNames() throws IOException {
		Process pb = new ProcessBuilder(List.of("/usr/bin/docker", "ps", "--format", "{{.Names}}")).start();
		BufferedReader reader =  new BufferedReader(new InputStreamReader(pb.getInputStream()));
		ArrayList<String> appNames = new ArrayList<>();
		String line;
		while ((line = reader.readLine()) != null) {
			appNames.add(line);
		}
		return appNames;
	}

	/**
	 * This method allows to stop a running application in docker. 
	 * 
	 * @param id
	 * 		id is the id of the application running in docker that you want to stop.
	 */
	public static void stop(String id) {
		List<String> command=  Arrays.asList("/usr/bin/docker", "stop", "-t", "0", id);
		ProcessBuilder pb= new ProcessBuilder(command);
		try {
			pb.start();
		} catch (IOException e) {
			System.out.println("Exception occurred when stoping docker");
		}
	}
}