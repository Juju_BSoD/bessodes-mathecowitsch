package jbes.gmat.square;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;

/**
 * This class allows the server to directly manage the operation of applications. 
 * @author J.Bessodes G.Mathecowitsch
 */
@Path("/auto-scale")
@Produces(MediaType.APPLICATION_JSON)
public class AutoScaleRessource {
	
	/**
	 * This method gives informations about the update needed to have the server working properly.
	 * @return
	 * 		this method returns a JSON Object with the informations needed.
	 */
	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
    public Response update() {
		JsonObject value = Json.createObjectBuilder()
			     .add("todomvc:8082", "need to start 1 instance(s)")
			     .add("demo:8083", "need to stop 2 instance(s)")
			.build();
		return Response.ok(value).build();
    }

	/**
	 * This method gives informations about the status of the differents instances.
	 * @return
	 * 		this method returns a JSON Object with the informations needed.
	 */
	@GET
	@Path("/status")
    public Response status() {
		JsonObject value = Json.createObjectBuilder()
			     .add("todomvc:8082", "no action")
			     .add("demo:8083", "need to stop 1 instance(s)")
			.build();
		return Response.ok(value).build();
    }

	/**
	 * This method allows to go back to the original state of the server.
	 * @return
	 * 		this method returns a JSON Object with the informations needed.
	 */
	@GET
	@Path("/stop")
    public Response stop() {
		JsonObject value = Json.createObjectBuilder()
			     .add("todomvc:8082", 2)
			     .add("demo:8083", 1)
			.build();
		return Response.ok(value).build();
    }
}
